package it.unibo.oop.supermario.world;
/**Defines SolidWorld interface.
*/
public interface SolidWorldCreator {
    /**Load each object of tiled map in its related class objects.
     * */
    void initSolidWorld();
}
