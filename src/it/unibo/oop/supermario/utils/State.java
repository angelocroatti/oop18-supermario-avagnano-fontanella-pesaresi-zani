package it.unibo.oop.supermario.utils;
/**
 *  The states of mario during the game.
 */
public enum State {
    /**
     * Falling state.
     */
    FALLING,

    /**
     * Jumping state.
     */
    JUMPING,

    /**
     * Standing state.
     */
    STANDING,

    /**
     * Running state.
     */
    RUNNING,

    /**
     * Growing state.
     */
    GROWING,

    /**
     * Fire state.
     */
    FIRE,

    /**
     * Crouching state.
     */
    CROUCHING,

    /**
     * Dying state.
     */
    DYING,

    /**
     * Firing state.
     */
    FIRING,

    /**
     * Shrinking state.
     */
    SHRINKING;
}
